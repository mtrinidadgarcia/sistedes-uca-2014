function refreshCountdown(periods) {
	var i, j = 0;
	var elements_arr = ['.days', '.hours', '.minutes', '.seconds'];

	for(i = 0; i < periods.length; i++){
		periods[i] = String('00' + periods[i].toString()).slice((i==0 ? -3 : -2));
		j = periods[i].length - 1;
		jQuery('#timer ' + elements_arr[i] + ' span').remove();
		while( j >= 0) {
			var digit = periods[i].charAt(j);
			jQuery('#timer ' + elements_arr[i]).prepend('<span>' + digit + '</span>');
			j--;
		}
	}
}
			
function numPadding (number, paddingChar,i) {
	var padding = new Array(number + 1).join(paddingChar);
	return padding.substr(0, padding.length - (Math.floor(i).toString().length)) + Math.floor(i );
}



$(function(){
	//en milisegundos
	var countdown_expiration = 1410883200000; 
	
	if(countdown_expiration > 0){
		var timestamp = countdown_expiration - Date.now();
		timestamp /= 1000;
		function component(x, v) {
			return Math.floor(x / v);
		}
		var $div = $('#timer .countdown');
		setInterval(function() {
			timestamp--;
			var days    = component(timestamp, 24 * 60 * 60),
			hours   = component(timestamp, 60 * 60) % 24,
			minutes = component(timestamp, 60) % 60,
			seconds = component(timestamp, 1) % 60;
			refreshCountdown([Math.abs(days), Math.abs(hours), Math.abs(minutes), Math.abs(seconds)]);
		}, 1000);
	}
	
	   
/*var newYear = new Date(); 
newYear = new Date(newYear.getFullYear() + 1, 1 - 1, 1); 
$('#defaultCountdown').countdown({until: newYear}); */
 
/*	$('#removeCountdown').click(function() { 
	var destroy = $(this).text() === 'Remove'; 
	$(this).text(destroy ? 'Re-attach' : 'Remove'); 
	$('#defaultCountdown').countdown(destroy ? 'destroy' : {until: newYear}); 
});
*/
	
			
$(".items").click(function(){
			
	$(".items").removeClass("active");
	$(this).addClass("active");
	
	$(".cal").css("display", "none");
	//$(".cal").hide("fast");
	
	if($(this).attr('id') == "id1"){
		
		$("#ide1").css("display", "");
		//$("#ide1").show("fast");
		
	}else if($(this).attr('id') == "id2"){
		
		$("#ide2").css("display", "");
		//$("#ide2").show("fast");
		
	}else if($(this).attr('id') == "id3"){
		
		$("#ide3").css("display", "");
		//$("#ide3").show("fast");
		
	}else if($(this).attr('id') == "id4"){
		
		$("#ide4").css("display", "");
		//$("#ide4").show("fast");
		
	}
});

	
});

$("#menu-close").click(function(e) {
	e.preventDefault();
	$("#sidebar-wrapper").toggleClass("active");
});

$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#sidebar-wrapper").toggleClass("active");
});


$(function() {
	$('a[href*=#]:not([href=#])').click(function() {
		
		var a = $(this).attr("id");
		
		//nos aseguramos que al pulsar el carousel no tengamos problemas de movernos de seccion
		if(a != "next" && a != "prev")
		{			
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		}
	});
	
	
	$('.nav li > .items').on('click', function(){

	  if($(".navbar-toggle").css("display") != "none")
		$(".navbar-toggle").click();

	});

});

