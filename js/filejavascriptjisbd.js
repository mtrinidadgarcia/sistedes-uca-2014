// JavaScript Document
  <!-- Custom JavaScript for the Side Menu and Smooth Scrolling -->	
$('#myTab a').click(function (e) {
	e.preventDefault();
	$(this).tab('show');
	//para cerrar el dropdown cuando pinchemos en el
	//$('[data-toggle="dropdown"]').parent().removeClass('open');
});

$('#trabReg').click(function (e) {
	e.preventDefault();
	$("#listEnvio").removeClass('active');
	$("#drop1").tab('show');
	
});


$('#trabInvest').click(function (e) {
	e.preventDefault();
	$("#listEnvio").removeClass('active');
	$("#drop1").tab('show');
	
});


$('#divulgTrab').click(function (e) {
	e.preventDefault();
	$("#listEnvio").removeClass('active');
	$("#drop1").tab('show');
	
});

$('#demostHerr').click(function (e) {
	e.preventDefault();
	$("#listEnvio").removeClass('active');
	$("#drop1").tab('show'); 
	
});

$("#menu-close").click(function(e) {
	e.preventDefault();
	$("#sidebar-wrapper").toggleClass("active");
});

$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#sidebar-wrapper").toggleClass("active");
});
    
$(function() {
	$('a[href*=#]:not([href=#])').click(function() 
	{
		
		var a = $(this).attr("id");
		
		//nos aseguramos que al pulsar el carousel no tengamos problemas de movernos de seccion
		if(a != "trabReg" && a != "demostHerr" && a != "divulgTrab" && a != "trabInvest" && a != "next" && a != "prev" && a != "drop0" && a != "drop1" && a != "drop2" &&  a != "drop3" && a != "drop4" && a != "drop5" && a != "drop6" && a != "drop7" && a != "descargar")
		{		
		
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		}
	
	});
	
	
	$('.nav li > .items').on('click', function(){

	  if($(".navbar-toggle").css("display") != "none")
		$(".navbar-toggle").click();

	});
});
